#pragma once

#include "Node.h"
#include "Element.h"

#include <vector>

class Mesh
{
public:
    Mesh() = default;
    void AddNode(Node* node)
    {
        m_nodes.push_back(node);
    }

    void AddElement(Element* element)
    {
        m_elements.push_back(element);
    }

    int GetNumNodes() const
    {
        return m_nodes.size();
    }

    int GetNumElements() const
    {
        return m_elements.size();
    }

    Node* GetNodeById(int id) const
    {
        for (const auto& node : m_nodes)
            if (id == node->GetId())
                return node;
        return nullptr;
    }

    Element* GetElementById(int id) const
    {
        for (const auto& element : m_elements)
            if (id == element->GetId())
                return element;
        return nullptr;
    }

    Node* GetNthValueNodes(int nodeId)
    {
        return m_nodes[nodeId];
    }

    std::vector<Element*> GetElements() const
    {
        return m_elements;
    }

    int GetLinesX()
    {
        return m_linesX;
    }

    int GetLinesY()
    {
        return m_linesY;
    }

    void SetLinesX(int linesX)
    {
        m_linesX = linesX;
    }

    void SetLinesY(int linesY)
    {
        m_linesY = linesY;
    }

private:
    std::vector<Node*> m_nodes;
    std::vector<Element*> m_elements;
    int m_linesX;
    int m_linesY;
};