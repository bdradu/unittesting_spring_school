#pragma once

#include <array>

using Int4 = std::array<int, 4>;
class Element
{
public:
    Element() = default;
    Element(int id, Int4 nodes):m_id(id), m_nodes(nodes)
    { 
    }

    void SetId(int id)
    {
        m_id = id;
    }

    int GetId() const
    {
        return m_id;
    }

    std::array<int, 4>& GetNodes()
    {
        return m_nodes;
    }

    void SetNthValueNodes(int nodeId, int value)
    {
        m_nodes[nodeId] = value;
    }

private:
    int m_id;
    Int4 m_nodes;
};