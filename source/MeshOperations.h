#pragma once

#include "Node.h"
#include "Element.h"
#include "Mesh.h"

#include <vector>

typedef std::vector<float> Vector3;

namespace Errors
{
    enum ErrorCode
    {
        OK = 0,
        BAD_VECTOR_X_VALUES,
        BAD_VECTOR_Y_VALUES,
        NEGATIVE_DIMENSIONS_VALUES,
        NO_MESH_SELECTED,
        NEGATIVE_MAX_ELEMENT_SIZE
    };
}

namespace Math {


    Vector3 VectorAddition(const Vector3& first, const Vector3& second)
    {
        return Vector3{ 
        first[0] + second[0],
        first[1] + second[1],
        first[2] + second[2]
        };
    }

    Vector3 SubtractionOfVectors(const Vector3& first, const Vector3& second)
    {
        return Vector3{ 
        first[0] - second[0],
        first[1] - second[1],
        first[2] - second[2]
        };
    }

    Vector3 ScalarVectorMultiplication(float scalar, const Vector3& second)
    {
        return Vector3{ scalar * second[0],
        scalar * second[1],
        scalar * second[2]
        };
    }

    float VectorMagnitude(const Vector3& vector3)
    {
        return std::sqrt(vector3[0] * vector3[0] + vector3[1] * 
                         vector3[1] + vector3[2] * vector3[2]);
    }

    Vector3 CrossProduct(const Vector3& first, const Vector3& second)
    {
        return Vector3{
            first[1] * second[2] - first[2] * second[1],
            first[2] * second[0] - first[0] * second[2],
            first[0] * second[1] - first[1] * second[0]
        };
    }

    Vector3 ColumnMatrixMultiplication(Vector3& column, const Vector3& row1, const Vector3& row2, const Vector3& row3)
    {
        return VectorAddition(
            VectorAddition(
                ScalarVectorMultiplication(column[0], row1),
                ScalarVectorMultiplication(column[1], row2)
            ),
            ScalarVectorMultiplication(column[2], row3)
        );
    }

    bool IsInteger(float number) {
        if (number != static_cast<int>(number))
            return false;
        else if(number == static_cast<int>(number))
            return true;
        return true;
    }
}

namespace MeshOperations {

    Mesh* m_activeMesh = nullptr;

    void SetActiveMesh(Mesh* mesh)
    {
        m_activeMesh = mesh;
    }

    void CreateNode(float x, float y, float z)
    {
        auto node = new Node();
        node->SetId(m_activeMesh->GetNumNodes() + 1);
        node->SetNthValueCsys(0, x);
        node->SetNthValueCsys(1, y);
        node->SetNthValueCsys(2, z);
        m_activeMesh->AddNode(node);
    }

    void CreateElement(const std::array<int, 4>& nodeIndexIds)
    {
        auto vctCopy = std::array<int, 4>(nodeIndexIds);
        for (auto& nodeIndexId : vctCopy)
            nodeIndexId -= 1;
        auto element = new Element();
        element->SetId(m_activeMesh->GetNumElements() + 1);
        element->SetNthValueNodes(0, nodeIndexIds[0]);
        element->SetNthValueNodes(1, nodeIndexIds[1]);
        element->SetNthValueNodes(2, nodeIndexIds[2]);
        element->SetNthValueNodes(3, nodeIndexIds[3]);
        m_activeMesh->AddElement(element);
    }


    double GetMaxElementSize(float lengthX, const float lengthY, const float maxElementSize)
    {
        double size = maxElementSize;

        while (!Math::IsInteger(lengthX / size) && !Math::IsInteger(lengthY / size))
        {
            size -= 0.01;
        }
        return size;
    }

    std::vector<int> GetElementsIdsRelatedToNode(const int searchedNodeId)
    {
        auto elementsIds = std::vector<int>();
        for (const auto& elem : m_activeMesh->GetElements())
            for (int i = 0; i < 4; i++)
                if (elem->GetNodes()[i] + 1 == searchedNodeId)
                    elementsIds.push_back(elem->GetId());
        return elementsIds;
    }

    Errors::ErrorCode CreateMesh(const Vector3 planeCenter, const float lengthX, const float lengthY, const float maxElementSize, const Vector3 newXVector, Vector3 newYVector)
    {
        if (maxElementSize < 1)
            return Errors::ErrorCode::NEGATIVE_MAX_ELEMENT_SIZE;
        int xVectorNull = 0;
        int yVectorNull = 0;
        for (int i = 0; i < 3; i++)
        {
            xVectorNull += newXVector[i] == 0.0 ? 1 : 0;
            yVectorNull += newYVector[i] == 0.0 ? 1 : 0;
        }
        if (xVectorNull == 3)
            return Errors::ErrorCode::BAD_VECTOR_X_VALUES;
        if (yVectorNull == 3)
            return Errors::ErrorCode::BAD_VECTOR_Y_VALUES;
        if (lengthX <= 0.0 || lengthY <= 0.0)
            return Errors::ErrorCode::NEGATIVE_DIMENSIONS_VALUES;

        double elementSize = GetMaxElementSize(lengthX, lengthY, maxElementSize);
        m_activeMesh->SetLinesX(lengthX / elementSize);
        m_activeMesh->SetLinesY(lengthY / elementSize);
        for (float x = 0; x <= lengthX; x += elementSize)
        for (float y = 0; y <= lengthY; y += elementSize)
                CreateNode(x, y, 0);
        int lineNumber = 1;
        for (int nodeIndex = 0; nodeIndex < m_activeMesh->GetNumNodes() - m_activeMesh->GetLinesY() - 1; nodeIndex++)
        {
            if (lineNumber * (m_activeMesh->GetLinesY() + 1) - 1 == nodeIndex)
            {
                lineNumber++;
                continue;
            }
            auto arr = std::array<int, 4>
            {
                nodeIndex,
                    nodeIndex + 1,
                        nodeIndex + 2 + m_activeMesh->GetLinesY(),
                            nodeIndex + 1 + m_activeMesh->GetLinesY()
            };

        auto vctCopy = std::array<int, 4>(arr);
        for (auto& nodeIndexId : vctCopy)
            nodeIndexId -= 1;
        auto element = new Element();
        element->SetId(m_activeMesh->GetNumElements() + 1);
        element->SetNthValueNodes(0, arr[0]);
        element->SetNthValueNodes(1, arr[1]);
        element->SetNthValueNodes(2, arr[2]);
        element->SetNthValueNodes(3, arr[3]);
        m_activeMesh->AddElement(element);
        }

        float planeStartX = planeCenter[0] - lengthY / 2.0;
        double planeStartY = planeCenter[1] - lengthX / 2.0;
        Vector3 newOrigin = { planeStartX , float(planeStartY), planeCenter[2] };

        Vector3 newZVector = Math::CrossProduct(newXVector, newYVector);

        for (int i = 1; i <= m_activeMesh->GetNumNodes(); i++) {
            auto node = m_activeMesh->GetNthValueNodes(i-1);
            auto newNodeCsys = Math::ColumnMatrixMultiplication(Vector3(node->GetCsys().begin(), node->GetCsys().end()), newXVector, newYVector, newZVector);
            newNodeCsys = Math::VectorAddition(newNodeCsys, newOrigin);
            m_activeMesh->GetNthValueNodes(i-1)->SetNthValueCsys(0, newNodeCsys[0]);
            m_activeMesh->GetNthValueNodes(i-1)->SetNthValueCsys(1, newNodeCsys[1]);

            m_activeMesh->GetNthValueNodes(i-1)->SetNthValueCsys(2, newNodeCsys[2]);
        }
        return Errors::ErrorCode::OK;
    };
};