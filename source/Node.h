#pragma once

#include <array>

using Float3 = std::array<float, 3>;

class Node {
public:
    Node() = default;
    Node(int id) :m_id(id) {}
    Node(int id, Float3 csys) :m_id(id), m_csys(csys) {}

    void SetId(int id)
    {
        m_id = id;
    };

    int GetId() const 
    { 
        return m_id;
    };

    void SetNthValueCsys(int csys, float value)
    {
        m_csys[csys] = value;
    };

    float GetNthValueCsys(int index)
    {
        return m_csys[index];
    }

    Float3& GetCsys()
    {
        return m_csys;
    }

private:
    int m_id;
    Float3 m_csys;
};