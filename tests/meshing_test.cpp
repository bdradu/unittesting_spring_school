#include <catch.hpp>
#include "MeshOperations.h"

using namespace Catch::Matchers;
using VectorF = std::vector<float>;

TEST_CASE("TestNodeAreCreatedWithTheCorrectId", "meshing")
{
    auto node = new Node();
    node->SetId(1);
    REQUIRE(node->GetId() == 1);
}

TEST_CASE("TestElementAreCreatedWithTheCorrectIds", "meshing")
{
    auto element = new Element();
    element->SetId(1);
    element->SetNthValueNodes(0, 1);
    element->SetNthValueNodes(1, 2);
    element->SetNthValueNodes(2, 3);
    element->SetNthValueNodes(3, 4);

    REQUIRE_THAT(std::vector<int>(element->GetNodes().begin(), element->GetNodes().end()), Equals(std::vector<int>{ 1, 2, 3, 4 }));
}

TEST_CASE("TestNodeCreationInMeshIsCorrect", "meshing")
{
    auto mesh = new Mesh();
    MeshOperations::SetActiveMesh(mesh);
    MeshOperations::CreateNode(0.0, 2.0, 0.0);
    REQUIRE(mesh->GetNodeById(1)->GetNthValueCsys(0) == Approx(0.0f).epsilon(1e-5));
}

TEST_CASE("TestElementCreationInMeshSetsTheCorrectNodeIdIndexes", "meshing")
{
    auto mesh = new Mesh();
    MeshOperations::SetActiveMesh(mesh);
    MeshOperations::CreateNode(0.0, 0.0, 0.0);
    MeshOperations::CreateNode(0.0, 5.0, 0.0);
    MeshOperations::CreateNode(5.0, 0.0, 0.0);
    MeshOperations::CreateNode(5.0, 5.0, 0.0);
    MeshOperations::CreateElement({ 1, 2, 3, 4 });

    REQUIRE_THAT(std::vector<int>(mesh->GetElementById(1)->GetNodes().begin(), mesh->GetElementById(1)->GetNodes().end()), Equals(std::vector<int>{1, 2, 3, 4}));
}

TEST_CASE("TestElementSizeIsBelowMaxElementSize", "meshing")
{
    double dimension = MeshOperations::GetMaxElementSize(100, 50, 5);
    REQUIRE(dimension <= 5);
}

TEST_CASE("TestNodesSizeInMeshCreation", "meshing")
{
    auto mesh = new Mesh();
    MeshOperations::SetActiveMesh(mesh);
    MeshOperations::CreateMesh(VectorF{ 25.0, 25.0, 0 }, 100.0, 20.0, 5.0, VectorF{ 1.0, 0.0, 0.0 }, VectorF{ 0.0,1.0,0.0 });
    REQUIRE(mesh->GetNumNodes() == 105);
}

TEST_CASE("TestElementsSizeAndNodeIndecesCreation", "meshing")
{
    auto mesh = new Mesh();
    MeshOperations::SetActiveMesh(mesh);
    MeshOperations::CreateMesh(VectorF{ 25, 25, 0 }, 10, 5, 1, VectorF{ 0.0, 1.0, 0.0 }, VectorF{ 1.0,0.0,0.0 });
    REQUIRE(mesh->GetNumNodes() == 66);
    REQUIRE(mesh->GetNumElements() == 50);
    REQUIRE_THAT(VectorF(mesh->GetElementById(50)->GetNodes().begin(), mesh->GetElementById(50)->GetNodes().end()), Equals(VectorF{ 58, 59, 65, 64 }));
}

TEST_CASE("Test90DegreeZRotationInSmallMesh", "meshing")
{
    auto mesh = new Mesh();
    MeshOperations::SetActiveMesh(mesh);
    MeshOperations::CreateMesh(VectorF{ 0, 0, 0 }, 1, 1, 1, VectorF{ 0.0, 1.0, 0.0 }, VectorF{ -1, 0.0, 0.0 });
    REQUIRE(mesh->GetNumNodes() == 4);
    REQUIRE(mesh->GetNumElements() == 1);
    REQUIRE_THAT(VectorF(mesh->GetElementById(1)->GetNodes().begin(), mesh->GetElementById(1)->GetNodes().end()), Equals(VectorF{ 0, 1, 3, 2 }));
    REQUIRE_THAT(VectorF(mesh->GetNodeById(1)->GetCsys().begin(), mesh->GetNodeById(1)->GetCsys().end()), Equals(VectorF{ -0.5,-0.5,0.0 }));
    REQUIRE_THAT(VectorF(mesh->GetNodeById(2)->GetCsys().begin(), mesh->GetNodeById(2)->GetCsys().end()), Equals(VectorF{ -1.5,-0.5,0.0 }));
    REQUIRE_THAT(VectorF(mesh->GetNodeById(3)->GetCsys().begin(), mesh->GetNodeById(3)->GetCsys().end()), Equals(VectorF{ -0.5,0.5,0.0 }));
    REQUIRE_THAT(VectorF(mesh->GetNodeById(4)->GetCsys().begin(), mesh->GetNodeById(4)->GetCsys().end()), Equals(VectorF{ -1.5,0.5,0.0 }));
}
